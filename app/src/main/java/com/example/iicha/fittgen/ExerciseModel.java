package com.example.iicha.fittgen;

public class ExerciseModel {

    private String description, name, repeat, series, group;

    public ExerciseModel(){

    }

    public ExerciseModel(String description, String name, String repeat, String series, String group) {
        this.description = description;
        this.name = name;
        this.repeat = repeat;
        this.series = series;
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
