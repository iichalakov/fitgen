package com.example.iicha.fittgen;

public class UploadImage {

    private String datePick;
    private String url;

    public UploadImage(){
        //
    }

    public UploadImage(String date,String imageUrl){
        if(date.trim().equals("")){
            date = "";
        }
        datePick = date;
        url = imageUrl;
    }

    public String getDatePick(){
        return datePick;
    }

    public void setDatePick(String date){
        datePick = date;
    }

    public String getImageUrl(){
        return url;
    }

    public void setUrl(String imageUrl){
        url = imageUrl;
    }


}
