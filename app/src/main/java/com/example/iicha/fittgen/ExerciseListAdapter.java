package com.example.iicha.fittgen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ExerciseListAdapter extends RecyclerView.Adapter<ExerciseListAdapter.ViewHolder> {

    public List<ExerciseModel> exerciseModelList;
    private LayoutInflater inflater;
    private Context context;

    public ExerciseListAdapter(Context context, List<ExerciseModel> exerciseModelList){
        this.exerciseModelList = exerciseModelList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.exercise_list_item,parent,false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("onBindView position: ", "" + position);

        ExerciseModel exerciseModel = exerciseModelList.get(position);
        Log.d("onBindView: ", "exerciseModel name: " + exerciseModel.getName());

            holder.exName.setText(exerciseModel.getName());
            holder.exGrp.setText(exerciseModel.getGroup());
        holder.exRepeat.setText(exerciseModel.getRepeat());
        holder.exSeries.setText(exerciseModel.getSeries());
        holder.exDescription.setText(exerciseModel.getDescription());

        Log.d("onBindView: ", "exerciseModel group: " + exerciseModel.getGroup());
       // }
    }

    @Override
    public int getItemCount() {
        return exerciseModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView exName;
        public TextView exGrp;
        public TextView exSeries;
        public TextView exRepeat;
        public TextView exDescription;


        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            exName = (TextView) mView.findViewById(R.id.ex_name);
            exGrp = (TextView) mView.findViewById(R.id.textView10);
            exRepeat = (TextView) mView.findViewById(R.id.textView11);
            exSeries = (TextView) mView.findViewById(R.id.textView12);
            exDescription = (TextView) mView.findViewById(R.id.exdescription);

        }
    }

}
