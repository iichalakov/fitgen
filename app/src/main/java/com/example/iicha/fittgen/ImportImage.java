package com.example.iicha.fittgen;


import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;



public class ImportImage extends AppCompatActivity {

    private static  final  int PICK_IMAGE_REQUEST = 1;
    private Button buttonChooseImage;
   // private Button buttonUploadImage;
    private EditText datePicker;
    private ImageView importImageView;
    private ProgressBar progressBarUpload;
    private FirebaseFirestore db;
    private StorageReference firebaseStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_image);
        setTitle("Import Image");

        buttonChooseImage = (Button) findViewById(R.id.button_choose_image);
       // buttonUploadImage = (Button) findViewById(R.id.button_upload_image);
        datePicker = (EditText) findViewById(R.id.date_pick_image);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate =df.format(c);
        datePicker.setText(formattedDate);
        datePicker.setKeyListener(null);

        importImageView = (ImageView) findViewById(R.id.imageView_upload_image);
        progressBarUpload = (ProgressBar) findViewById(R.id.progress_bar_upload_image);

        firebaseStorage = FirebaseStorage.getInstance().getReference();
        db = FirebaseFirestore.getInstance();

        buttonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });


//        buttonUploadImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }//END ONCREATE METHOD


    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null ){
             uri = data.getData();

             final StorageReference filepath= firebaseStorage.child("Uploads").child(uri.getLastPathSegment());

             UploadTask uploadTask = filepath.putFile(uri);
             Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                 @Override
                 public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        Log.e("ImportImage: ", task.getException().toString());
                    }

                     return filepath.getDownloadUrl();
                 }
             }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                 @Override
                 public void onComplete(@NonNull Task<Uri> task) {
                     if(task.isSuccessful()){
                         final Uri uri = task.getResult();
                         String pickdate = datePicker.getText().toString();

                         Map<String,Object> impimgMap = new HashMap<>();
                         impimgMap.put("uri",uri.toString());
                         impimgMap.put("date",pickdate);


                         db.collection("Images").add(impimgMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                             @Override
                             public void onSuccess(DocumentReference documentReference) {
                                 Toast.makeText(ImportImage.this,"Upload Complete", Toast.LENGTH_LONG).show();
                             }
                         }).addOnFailureListener(new OnFailureListener() {
                             @Override
                             public void onFailure(@NonNull Exception e) {
                                 String error = e.getMessage();
                                 Toast.makeText(ImportImage.this,"Error !" + error,Toast.LENGTH_LONG).show();
                             }
                         });
                         Picasso.with(ImportImage.this).load(uri).into(importImageView);
                     }
                 }
             });



//            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    Toast.makeText(ImportImage.this,"Upload Complete",Toast.LENGTH_SHORT).show();
//                }
//
//
//            });

//            String saveuri = uri.getPathSegments().toString();
//            String pickdate = datePicker.getText().toString();
//
//            Map<String, Object> impImgMap = new HashMap<>();
//
//           // Map<String, String> imageMap = new HashMap<>();
//            //imageMap.put("");
//
//            //.put("ImageDate",pickdate);
//            impImgMap.put("ImageUri",saveuri);
//            impImgMap.put("ImageURL",pickdate);
//
//            db.collection("Images").add(impImgMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                @Override
//                public void onSuccess(DocumentReference documentReference) {
//                    Toast.makeText(ImportImage.this, "Added to Firestore",Toast.LENGTH_SHORT).show();
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    String error = e.getMessage();
//                    Toast.makeText(ImportImage.this, "Error !" + error,Toast.LENGTH_SHORT).show();
//                }
//            });
//            Picasso.with(this).load(uri).into(importImageView);
        }
    }//end onActivityResult
//Work if normal database
//    private String getFileExtension(Uri uri){
//        ContentResolver cR = getContentResolver();
//        MimeTypeMap mime = MimeTypeMap.getSingleton();
//        return mime.getExtensionFromMimeType(cR.getType(uri));
//    }
//
//    private void uploadFile() {
//        if(imageUri != null){
//            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis() + "." + getFileExtension(imageUri));
//
//            fileReference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressBarUpload.setProgress(0);
//                        }
//                    },500);
//                    Toast.makeText(ImportImage.this,"Upload Successful",Toast.LENGTH_SHORT).show();
//                    UploadImage uploadImage = new UploadImage(datePicker.getText().toString().trim(), taskSnapshot.getDownloadUrl().toString());
//                    String uploadId = mDatabaseRef.push().getKey();
//                    mDatabaseRef.child(uploadId).setValue(uploadImage);
//
//
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(ImportImage.this,e.getMessage(),Toast.LENGTH_SHORT).show();
//                }
//            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                    double progress = (100.0 * taskSnapshot.getBytesTransferred()/ taskSnapshot.getTotalByteCount());
//                    progressBarUpload.setProgress((int) progress);
//                }
//            });
//
//        }else{
//            Toast.makeText(this,"No file selected",Toast.LENGTH_SHORT).show();
//        }
//    }
}//END CLASS
