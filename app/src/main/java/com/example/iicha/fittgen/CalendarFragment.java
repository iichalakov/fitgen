package com.example.iicha.fittgen;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.widget.Toast.*;


/**
 * A simple {@link Fragment} subclass.
 */

public class CalendarFragment extends Fragment  {

    int workdays[] = {10,10,10,10,10,10,10,15,20,10,11,12};
    String monthNames[] ={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    int weight[] = {100,100,100,100,100,100,100,150,200,100,110,120};

    private Toolbar toolbar3;
    CompactCalendarView compactCalendarView;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM- yyyy", Locale.getDefault());

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_calendar, container, false);
            toolbar3 = (Toolbar) v.findViewById(R.id.toolbar3);


        compactCalendarView = (CompactCalendarView) v.findViewById(R.id.compactcalendar_view);
        compactCalendarView.setUseThreeLetterAbbreviation(true);


        Event ev1 = new Event(Color.GREEN, 1477040400000L, "ASD");
        compactCalendarView.addEvent(ev1);

        Event ev2 = new Event(Color.GREEN, 1477040400000L, "Back Workout");
        compactCalendarView.addEvent(ev2);

        Event ev3 = new Event(Color.RED, 1477990800000L, "lEG Workout");
        compactCalendarView.addEvent(ev3);

        toolbar3.setTitle(simpleDateFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        List<Event> events = compactCalendarView.getEvents(1433701251000L);

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                Context context = getActivity().getApplicationContext();

                if (dateClicked.toString().compareTo("Fri Sep 28 00:00:00 AST 2018") == 0) {
                    Toast.makeText(context, "Back Workout", Toast.LENGTH_SHORT).show();

                } else if (dateClicked.toString().compareTo("Sun Oct 31 00:00:00 AST 2018") == 0) {
                    Toast.makeText(context, "Church Service & Flag Raising", Toast.LENGTH_SHORT).show();

                } else if (dateClicked.toString().compareTo("Wed Nov 02 00:00:00 AST 2016") == 0) {
                    Toast.makeText(context, "Bajan Display of Artifacts", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context, "Groups : Chest, Legs", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                toolbar3.setTitle(simpleDateFormat.format(firstDayOfNewMonth));
            }

            });
            gotoToday();
        return v;
    }


    public void gotoToday(){
        compactCalendarView.setCurrentDate(Calendar.getInstance(Locale.getDefault()).getTime());
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

    }



    private void openDialog() {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getFragmentManager(),"example dialog");
    }

    private void openWeightDialog(){

        WeightDialog weightDialog = new WeightDialog();
        weightDialog.show(getFragmentManager(),"weight dialog");

    }



}
