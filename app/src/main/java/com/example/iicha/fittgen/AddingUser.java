package com.example.iicha.fittgen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddingUser extends AppCompatActivity {

    private EditText addUserName;
    private EditText addUserDateOfBirth;
    private Button addUserToBase;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_user);

        setTitle("User Info");
        addUserName = (EditText) findViewById(R.id.addUserName);
        addUserDateOfBirth = (EditText) findViewById(R.id.addingDateofBirth);
        addUserToBase = (Button) findViewById(R.id.button);

        db = FirebaseFirestore.getInstance();

        addUserToBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = addUserName.getText().toString();
                String dateofbirth = addUserDateOfBirth.getText().toString();


                Map<String, String> userMap = new HashMap<>();

                userMap.put("UserName", username);
                userMap.put("DateOfBirth", dateofbirth);

                db.collection("UserInfo").add(userMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(AddingUser.this, "User Added to Firestore",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(AddingUser.this, "Error !" + error,Toast.LENGTH_SHORT).show();
                    }
                });

                Intent tabbedIntent = new Intent(AddingUser.this,TabbedMain.class);
                startActivity(tabbedIntent);
                finish();

            }//end onClick
        });//End onClickListener
    }//end OnCreate
}//end class
