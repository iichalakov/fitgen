package com.example.iicha.fittgen;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GalleryAdapter  extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public List<GalleryModel> galleryList;
    private LayoutInflater inflater;
    private Context context;

    public GalleryAdapter(Context context, List<GalleryModel> galleryList){
        this.galleryList = galleryList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.gallery_item,parent,false);
        GalleryAdapter.ViewHolder holder = new GalleryAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder holder, final int position) {
        Log.d("onBindView position: ", "" + position);

        GalleryModel galleryModel = galleryList.get(position);
        Log.d("onBindView: ", "URL: " + galleryModel.getUri());

        //Picasso.with(context).load(galleryModel.getUri()).into(holder.galleryview);
        Glide.with(context).load(galleryModel.getUri()).apply(new RequestOptions().centerCrop()).into(holder.galleryview);

//        holder.galleryview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GalleryModel galleryModel = galleryList.get(position);
//                Intent intent = new Intent(context, someClass.class);
//                intent.putExtra("imgUrl", galleryModel.getUri());
//                ...
//                String url = getIntent().getExtra("imgUrl");
//                //galleryModel . url, userID
//            }
//        });
//        Log.d("onBindView: ", "exerciseModel group: " + exerciseModel.getGroup());
        // }
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public ImageView galleryview;


        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            galleryview = (ImageView) mView.findViewById(R.id.galleryImageView);
        }
    }
}
