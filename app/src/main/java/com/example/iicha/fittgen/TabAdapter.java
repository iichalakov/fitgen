package com.example.iicha.fittgen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

public class TabAdapter extends FragmentStatePagerAdapter{
    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    String [] tabsTitle = new String[]{"Workout","Calendar","Statistics"};


    @Override

    public  CharSequence getPageTitle(int position){
        return  tabsTitle[position];
    }

    public Fragment getItem(int position) {
        switch (position){

            case 0:
                WorkoutFragment workoutFragment = new WorkoutFragment();
                return workoutFragment;
            case 1:
                CalendarFragment calendarFragment = new CalendarFragment();
                return calendarFragment;
            case 2:
                StatisticsFragment statisticsFragment = new StatisticsFragment();
                return statisticsFragment;
        }//end switch

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
