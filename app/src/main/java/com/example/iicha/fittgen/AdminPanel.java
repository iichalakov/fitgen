package com.example.iicha.fittgen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminPanel extends AppCompatActivity {

    private Button gotoex;
    private Button gotogroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);

        gotoex = (Button) findViewById(R.id.adminGotoex);
        gotogroup = (Button) findViewById(R.id.addgroup);

            gotoex.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent exIntent = new Intent(AdminPanel.this,AddingExercise.class);
                    startActivity(exIntent);
                    finish();
                }
            });
        gotogroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent exIntent = new Intent(AdminPanel.this,AddingMuscleGroups.class);
                startActivity(exIntent);
                finish();
            }
        });
    }
}
