package com.example.iicha.fittgen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddingInputParams extends AppCompatActivity {

    private FirebaseFirestore db;
    private EditText addWeight;
    private EditText addHeight;
    private Button createProgram;
    private Spinner dropdownPurpose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_input_params);

        db = FirebaseFirestore.getInstance();

        addWeight = (EditText) findViewById(R.id.addWeight);
        addHeight = (EditText) findViewById(R.id.addHeight);
        createProgram = (Button) findViewById(R.id.button2);
        dropdownPurpose = (Spinner) findViewById(R.id.spinner2);

        List<String> purposeLsit = new ArrayList<>();
        purposeLsit.add("Lose weight");
        purposeLsit.add("Gain weight");
        purposeLsit.add("Hold weight");

        ArrayAdapter<String> purposeAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,purposeLsit);
        purposeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdownPurpose.setAdapter(purposeAdapter);

        dropdownPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemvalue = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });//end dropdown

        createProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String height = addHeight.getText().toString();
                String weight = addWeight.getText().toString();
                String purpose = dropdownPurpose.getSelectedItem().toString();

                Map<String,String> paramMap = new HashMap<>();
                paramMap.put("Height",height);
                paramMap.put("Weight",weight);
                paramMap.put("Purpose",purpose);

                db.collection("InputParams").add(paramMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(AddingInputParams.this, "Params Added to Firestore",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(AddingInputParams.this, "Error !" + error,Toast.LENGTH_SHORT).show();
                    }
                });

                //refresh.onRefresh();
                Intent tabbedIntent = new Intent(AddingInputParams.this,TabbedMain.class);
                startActivity(tabbedIntent);
            }
        });



    }
    public interface Create{
        void onRefresh();
    }
    public Create refresh;

    public void setOnDataChangeListener(Create create){
        this.refresh = create;
    }
}
