package com.example.iicha.fittgen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

public class RegisterActivity extends AppCompatActivity {

    private EditText mEmail;
    private EditText mPassword;
    private Button mCreateButton;

    private ProgressDialog mRegProgress;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle("Creating Account");
        //ProgressDialog

        mRegProgress = new ProgressDialog(this);

        //Firebase Aut + int setting fields
        mAuth = FirebaseAuth.getInstance();
        mEmail = (EditText) findViewById(R.id.reg_email);
        mPassword = (EditText) findViewById(R.id.reg_password);
        mCreateButton = (Button) findViewById(R.id.reg_create_button);

        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                if(!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)){

                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait while we create your account !");
                    mRegProgress.show();

                    register_user(email,password);
                }
                else{
                    Toast.makeText(RegisterActivity.this,"Please check your information.",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void register_user(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    mRegProgress.dismiss();

//                    Intent tabbedIntent = new Intent(RegisterActivity.this,TabbedMain.class);
//                    tabbedIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Intent addUser = new Intent(RegisterActivity.this,AddingUser.class);
                    addUser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(addUser);
                    finish();
                }
                else {
                    mRegProgress.hide();
                    Toast.makeText(RegisterActivity.this,"Cannot Sign in. Please check your information.",Toast.LENGTH_LONG).show();
                }
            }
        });
    }//END REGISTER_USER
}//END OF CLASS
