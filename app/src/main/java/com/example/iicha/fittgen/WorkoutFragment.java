package com.example.iicha.fittgen;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutFragment extends Fragment {
    private static final String TAG = "Firelog";
    private RecyclerView mainList;
    private List<ExerciseModel> exerciseModelList;
    private ExerciseListAdapter exerciseListAdapter;
    private FirebaseFirestore db;

    public WorkoutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_workout, container, false);
        db = FirebaseFirestore.getInstance();
        exerciseModelList = new ArrayList<>();
        exerciseListAdapter = new ExerciseListAdapter(getActivity(), exerciseModelList);
        mainList = (RecyclerView) v.findViewById(R.id.tttt);
        mainList.setHasFixedSize(true);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        mainList.addItemDecoration(itemDecoration);
        mainList.setAdapter(exerciseListAdapter);
        //mainList.setVisibility(View.GONE);
        Log.d(TAG,"Firelog"+ mainList);
        db.collection("Exercise").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                    if(e != null){
                        Log.d(TAG,"Error" + " " + e.getMessage());
                    }

                for(DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()){

                    if(doc.getType() == DocumentChange.Type.ADDED){
                        Log.d(TAG,"doc : " + doc.getDocument().getData());
                        //Log.d(TAG,"Added doc to object : " + doc.getDocument().toObject(ExerciseModel.class));
                        ExerciseModel exerciseModel = doc.getDocument().toObject(ExerciseModel.class);
                        //Log.d(TAG,"exerciseModel " + exerciseModel.getName());
                        exerciseModelList.add(exerciseModel);
                        exerciseListAdapter.notifyDataSetChanged();
                    }//end IF
                    //  Log.d(TAG,"Description : " + exdescription );
                }//END FOR
                for(DocumentSnapshot as : queryDocumentSnapshots){

                    String exdescription = as.getString("description");

                    Log.d(TAG,"Description : " + exdescription );
                } //END FOR

            }//end of onEvent
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }






}//End fragment

