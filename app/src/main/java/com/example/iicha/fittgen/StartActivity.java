package com.example.iicha.fittgen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import junit.framework.Test;

public class StartActivity extends Activity {

    private Button mRegBtn;
    private  Button mLogBtn;
    private Button gotoGroup;
    private Button calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mRegBtn = (Button) findViewById(R.id.start_reg_button);
        mLogBtn = (Button) findViewById(R.id.startLoginButton);
        gotoGroup = (Button) findViewById(R.id.gotoGroup);
        calendar = (Button) findViewById(R.id.button3);

        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent reg_intent = new Intent(StartActivity.this, RegisterActivity.class);
                Intent reg_intent = new Intent(StartActivity.this,RegisterActivity.class);
                startActivity(reg_intent);

            }
        });

        mLogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log_intent = new Intent(StartActivity.this,LoginActivity.class);
                startActivity(log_intent);
            }
        });
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testcalendar = new Intent(StartActivity.this,AddingExercise.class);
                startActivity(testcalendar);
            }
        });

//        gotoGroup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent addingGroup_intent = new Intent(StartActivity.this,AddingMuscleGroups.class);
//                startActivity(addingGroup_intent);
//            }
//        });

    }
}
