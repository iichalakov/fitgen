package com.example.iicha.fittgen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Gallery extends AppCompatActivity {

    private RecyclerView galleryListView;
    private List<GalleryModel> galleryModels;
    private GalleryAdapter galleryAdapter;
    private FirebaseFirestore db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        setTitle("Gallery");
        db = FirebaseFirestore.getInstance();
        galleryListView = (RecyclerView) findViewById(R.id.galleryview);
        galleryModels = new ArrayList<>();
        galleryAdapter = new GalleryAdapter(getApplicationContext() ,galleryModels);

        galleryListView.setHasFixedSize(true);
        galleryListView.setAdapter(galleryAdapter);
        galleryListView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));

        db.collection("Images").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                if(e != null){
                    //Log.(TAG,"Error" + " " + e.getMessage());
                }
                for(DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()){

                    if(doc.getType() == DocumentChange.Type.ADDED){
                        GalleryModel galleryModel = doc.getDocument().toObject(GalleryModel.class);
                        galleryModels.add(galleryModel);
                        galleryAdapter.notifyDataSetChanged();
                    }//end IF
                    //  Log.d(TAG,"Description : " + exdescription );
                }//END FOR
            }
        });


    }
}
