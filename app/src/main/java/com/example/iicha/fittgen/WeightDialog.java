package com.example.iicha.fittgen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WeightDialog extends AppCompatDialogFragment {


    private FirebaseFirestore db;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        db = FirebaseFirestore.getInstance();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.layout_weightdialog,null);

        final EditText dateText = view.findViewById(R.id.datePlainText);

        builder.setView(view)
                .setTitle("Add to Statistics")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //add the date to firebase
                        Map<String,String> statisticWeight = new HashMap<>();
                        String weight = dateText.getText().toString();
                        statisticWeight.put("How much did is your weight this month",weight);

                        db.collection("WeightThisMonth").add(statisticWeight).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                               //need to add some logs
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                               //need to add some logs
                            }
                        });
                    }
                });


        return builder.create();

    }
}
