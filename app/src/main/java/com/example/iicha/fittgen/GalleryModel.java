package com.example.iicha.fittgen;

public class GalleryModel {

    private String uri,user;

    public GalleryModel(){

    }

    public GalleryModel(String uri, String user) {
        this.uri = uri;
        this.user = user;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
