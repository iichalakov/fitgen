package com.example.iicha.fittgen;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddingMuscleGroups extends AppCompatActivity {

    private EditText addGroupName;
    private EditText addGroupDescription;
    private Button addToBaseButton;

     private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_muscle_groups);

        addToBaseButton = (Button) findViewById(R.id.addFireGroup);
        addGroupName = (EditText) findViewById(R.id.addNameGroup);
        addGroupDescription = (EditText) findViewById(R.id.addDescriptionGroup);

       db = FirebaseFirestore.getInstance();


        addToBaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String name = addGroupName.getText().toString();
                String description = addGroupDescription.getText().toString();

                Map<String, String> groupMap = new HashMap<>();

                groupMap.put("groupName",name);
                groupMap.put("groupDescription",description);

                db.collection("groupName").add(groupMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(AddingMuscleGroups.this, "Group Added to Firestore",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(AddingMuscleGroups.this, "Error !" + error,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }
}
