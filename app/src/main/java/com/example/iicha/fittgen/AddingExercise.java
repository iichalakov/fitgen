package com.example.iicha.fittgen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddingExercise extends AppCompatActivity {
    private FirebaseFirestore db;
    private EditText exName;
    private EditText exDescription;
    private EditText exRepeat;
    private EditText exSeries;
    private Button addExtoBase;
    private Spinner dropdownPurpose;
    private Spinner dropdownPurpose2;
    private EditText groupName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_exercise);

        db = FirebaseFirestore.getInstance();

        exName = (EditText) findViewById(R.id.addToBaseEx);
        exDescription = (EditText) findViewById(R.id.addToBaseDescription);
        exRepeat = (EditText) findViewById(R.id.addToBaseRepetition);
        exSeries = (EditText) findViewById(R.id.addToBaseSeries);
        addExtoBase = (Button) findViewById(R.id.addExToBaseButton);
        dropdownPurpose = (Spinner) findViewById(R.id.spinner3);
        groupName = (EditText) findViewById(R.id.g);
        dropdownPurpose2 = (Spinner) findViewById(R.id.spinner4);
        groupName.setKeyListener(null);


        List<String> groupList = new ArrayList<>();
        groupList.add("Chest");
        groupList.add("Back");
        groupList.add("Biceps");
        groupList.add("Triceps");
        groupList.add("Legs");
        groupList.add("Stomach");

        List<String> purposeList = new ArrayList<>();
        purposeList.add("Lose weight");
        purposeList.add("Gain weight");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,purposeList );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdownPurpose2.setAdapter(adapter);

        ArrayAdapter<String> purposeAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,groupList);
        purposeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdownPurpose.setAdapter(purposeAdapter);

        dropdownPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemvalue = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });//end dropdown

        addExtoBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = exName.getText().toString();
                String description = exDescription.getText().toString();
                String repeat = exRepeat.getText().toString();
                String series = exSeries.getText().toString();
                String group = dropdownPurpose.getSelectedItem().toString();

                Map<String,String> exMap = new HashMap<>();
                exMap.put("name",name);
                exMap.put("description",description);
                exMap.put("repeat",repeat);
                exMap.put("series",series);
                exMap.put("group",group);


                db.collection("Exercise").add(exMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(AddingExercise.this, " Added to Firestore",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(AddingExercise.this, "Error !" + error,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
